package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class FlightsPage {

    WebDriver driver;

    By changeDateCarrouselNext = By.xpath("//button[@data-e2e='carousel-next']");
    By departureDayUpdate = By.xpath("//span[text()='06']");
    By departureMonthUpdate = By.xpath("//span[text()='Dec']");
    By selectDepartureFlight = By.xpath("//flight-card[@data-e2e='flight-card--outbound'][1]");
    By destinationDayUpdate = By.xpath("//span[text()='12']");
    By destinationMonthUpdate = By.xpath("//span[text()='Dec']");
    By changeDateCarrouselPrevious = By.xpath("(//button[@data-e2e='carousel-prev'])[2]//carousel-arrow");
    By selectDestinationFlight = By.xpath("//flight-card[@data-e2e='flight-card--inbound'][1]");
   By valueFlightOption = By.xpath("//span[contains(text(),' Continue for')]");
    By continueValuePopup = By.xpath("//button[contains(text(),' Continue with Value fare')]");
    By loginLater = By.xpath("//span[contains(text(),'Log in later')]");
    By titleBtn = By.xpath("(//ry-dropdown)[1]");
    By titleBtn2 = By.xpath("(//ry-dropdown)[2]");
    By title1 = By.xpath("//div[text()='Mr']");
    By firstName1 = By.id("form.passengers.ADT-0.name");
    By lastName1 =  By.id("form.passengers.ADT-0.surname");
    By title2 = By.xpath("//div[text()='Mr']");
    By firstName2 = By.id("form.passengers.ADT-1.name");
    By lastName2 =  By.id("form.passengers.ADT-1.surname");
    By firstName3 = By.id("form.passengers.CHD-0.name");
    By lastName3 =  By.id("form.passengers.CHD-0.surname");
    By continueBtn =  By.xpath("//button[contains(text(),'Continue')]");
    By familySettingPopup = By.xpath("//button[contains(text(),'Okay, got it.')]");


    public FlightsPage(WebDriver driver){
        this.driver=driver;
    }

    public void changeDates() throws InterruptedException {
        changeDate(departureDayUpdate,departureMonthUpdate,"right",selectDepartureFlight);
        changeDate(destinationDayUpdate,destinationMonthUpdate,"left",selectDestinationFlight);

    }

    public void changeDate(By dayChange, By monthChange, String direction, By flightType) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(changeDateCarrouselNext));

        //WebElement month =driver.findElement(departureMonthUpdate);

        while(!elementPresent(monthChange)){

            carrouselMoveRightLeft(direction);
        }

        while(!elementPresent(dayChange)){

            carrouselMoveRightLeft(direction);

        }

        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(dayChange));

        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(dayChange));
        Thread.sleep(1000);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);


        //wait = new WebDriverWait(driver,30);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(monthChange));

        driver.findElement(dayChange).click();

        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(flightType));

        driver.findElement(flightType).click();



    }


    public boolean elementPresent(By date){

        return driver.findElements(date).size()>0;

    }

    public void carrouselMoveRightLeft(String direction){
        Actions act = new Actions(driver);
        //prevArrow.click();
        if(direction.equals("right")){
            driver.findElement(changeDateCarrouselNext).click();
        }
        else{
            WebElement prevArrow = driver.findElement(changeDateCarrouselPrevious);
            act.moveToElement(prevArrow);
            driver.findElement(changeDateCarrouselPrevious).click();

        }
    }

    public void selectValueOption() {

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(valueFlightOption));

        driver.findElement(valueFlightOption).click();

        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(continueValuePopup));

        driver.findElement(continueValuePopup).click();

        wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginLater));

        driver.findElement(loginLater).click();

    }

    public void populatePassengersForm() {


        driver.findElement(titleBtn).click();

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(title1));

        driver.findElement(title1).click();
        driver.findElement(firstName1).sendKeys("Sónia");
        driver.findElement(lastName1).sendKeys("Pereira");
        driver.findElement(titleBtn2).click();
        driver.findElement(title2).click();
        driver.findElement(firstName2).sendKeys("Diogo");
        driver.findElement(lastName2).sendKeys("Bettencourt");
        driver.findElement(firstName3).sendKeys("Inês");
        driver.findElement(lastName3).sendKeys("Marçal");
        driver.findElement(continueBtn).click();

    }

    public void selectSeats() throws InterruptedException {

        Thread.sleep(4);
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(familySettingPopup));

        driver.findElement(familySettingPopup).click();
    }

    public void selectPackage() {
    }
}
