package pages;

import com.sun.xml.internal.ws.server.DefaultResourceInjector;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {

    WebDriver driver;
    By yesIAgreeBtn = By.className("cookie-popup-with-overlay__button");
    By fromDestination = By.id("input-button__departure");
    By airportDeparture = By.xpath("//span[contains(text(),'Lisbon')]");
    By toDestination = By.id("input-button__destination");
    By airportDestination = By.xpath("//span[contains(text(),'Paris Beauvais')]");
    By departureDate = By.xpath("//div[@data-id='2021-10-06']");
    By destinationDate = By.xpath("//div[@data-id='2021-10-30']");
    By passengersAdultBtn =  By.xpath("//div[text()='Adults']/../following-sibling::div//div[contains(@data-ref,'increment')]");
    By passengersChildBtn = By.xpath("//div[text()='Children']/../following-sibling::div//div[contains(@data-ref,'increment')]");
    By searchBtn = By.xpath("//ry-spinner[text()=' Search ']");



    public MainPage(WebDriver driver){
        this.driver=driver;
    }

    public void navigateToMainPage(){

        driver.get("https://www.ryanair.com/gb/en");

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(yesIAgreeBtn));

        driver.findElement(yesIAgreeBtn).click();

    }

    public void searchFlight() throws InterruptedException {

        //Select of departure and destination city
        driver.findElement(fromDestination).click();
        driver.findElement(fromDestination).clear();
        driver.findElement(fromDestination).sendKeys("Lisbon");

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(airportDeparture));

        driver.findElement(airportDeparture).click();

        driver.findElement(toDestination).click();
        driver.findElement(toDestination).clear();
        driver.findElement(toDestination).sendKeys("Paris Beauvais");

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(airportDestination));

        driver.findElement(airportDestination).click();

        //Select dates and passengers


        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(departureDate));

        driver.findElement(departureDate).click();
        //driver.findElement(passengers).click();

        driver.findElement(destinationDate).click();

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(passengersAdultBtn));

        driver.findElement(passengersAdultBtn).click();

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(passengersAdultBtn));

        driver.findElement(passengersChildBtn).click();
        //Click search flight
        driver.findElement(searchBtn).click();

    }
}
