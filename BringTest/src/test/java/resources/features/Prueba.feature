

Feature: Flights
  Scenario: Book a flight
    Given Access to the flights page
    When Search for a trip
    And Change the departure date
    And Fill the passenger's details
    Then Select the seats and package

