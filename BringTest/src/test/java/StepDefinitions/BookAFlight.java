package StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.FlightsPage;
import pages.MainPage;

public class BookAFlight {

    WebDriver driver = new ChromeDriver();
    MainPage mp = new MainPage(driver);
    FlightsPage fp = new FlightsPage(driver);

    @Given("Access to the flights page")
    public void access_to_the_flights_page() {

        mp.navigateToMainPage();

    }

    @When("Search for a trip")
    public void search_for_a_trip() throws InterruptedException {

        mp.searchFlight();
    }

    @And("Change the departure date")
    public void change_the_departure_date() throws InterruptedException {

        fp.changeDates();
        fp.selectValueOption();

    }

    @And("Fill the passenger's details")
    public void fill_the_passengers_details(){

        fp.populatePassengersForm();
    }

    @Then("Select the seats and package")
    public void select_the_seats_and_package() throws InterruptedException {

        fp.selectSeats();
        fp.selectPackage();
    }
}
